clear all; clc; close all;

%% Nominal configuration :

m = 7063; %mass : kg
g = 9.81;
X_bar_CG = 0.22;% gravity center's location
I_xx = 4951.7; %kg m^2
I_yy = 108511; %kg m^2
I_zz = 111224; %kg m^2

%% flight condition : 
H = 12192; %m
rho = 0.302; %kg/m^3
U0 = 235.9; %m/s
M = 0.8;
theta_0 = 7.5; %deg

S = 18.58; %m^2
b = 6.82; %m
c = 3.13; %m
AR = 2.5;
epsilon = 0.93;

%% Steady state condition

CL0 = 0.42;
CD0 = 0.13;
CTx0 = 0.025;

Cm0 = 0;
Cmt0 = 0;

Cm_u = -0.1;
Cm_alpha = -1;
Cm_alpha_dot = -1.6;
C_m_q = -12.5;
C_m_t_u = 0;
C_m_t_alpha = 0;
C_m_M = 0;
C_L_u = 0;
C_L_alpha = 4;
C_L_alpha_dot = 0;
C_L_q = 0;

C_D_u = 0;
C_D_M = 0;
C_D_alpha = 0.8;
C_D_alpha_dot = 0;
C_D_q = 0;

C_T_u = 0;
C_L_delta_e = 1.1;
C_D_delta_e = 0;
C_m_delta_e = -1.5;

C_L_delta_t = 0;
C_D_delta_t = 0;
C_m_delta_t = 0;

q_bar = 0.5 * rho * U0^2;

save("constants.mat");


