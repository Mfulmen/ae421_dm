clear all;
close all;
clc;

%% Constant data

% Conversion ratios
feet = 0.3048;
feet2 = 0.092903;
slug = 515.383;
lb = 0.454;
degree = pi / 180;
kelvin = 273.15;
s = tf('s');

% Flight conditions
altitude = 12192;    % m
air_density = 0.302; % kg/m^3
U0 = 235.9;          % m/s - Aircraft speed
M = 0.8;             % Mach number
theta_0 = 7.5;       % deg

% Physical constants
R = 287.058;          % J/kg/K - Air gas constant
g = 9.80665;             % m/s^2 - Standard gravity
gamma = 1.4;          % Heat capacity ratio
T_std = 15 + kelvin;  % K - Standard temperature

% At flight altitude
P = pressure(altitude);    % Pascal - Pressure at flight altitude
T = temperature(altitude); % K - Temperature at flight altitude
a = sqrt(gamma * R * T);   % m/s - Speed of sound
M_verif = U0 / a;

disp(['Mach given: ', num2str(M), ' - Mach calculated: ', num2str(M_verif)]);

% Nominal configuration
m = 7063;      % kg - Aircraft mass
X_cg = 0.22;   % ?  - Gravity center location
I_xx = 4951.7; % kg*m^2
I_yy = 108511; % kg*m^2
I_zz = 111224; % kg*m^2
I_xz = 122;    % kg*m^2

% Geometric data
S = 18.58;    % m^2 - Wing area
b = 6.82;     % m - Wing span
c_bar = 3.13; % m - Wing mean chord
AR = 2.5;     % Aspect ratio
e = 0.93;     % Wing oswald number

% Steady state conditions
CL_0 = 0.42;
CD_0 = 0.13;
CTx0 = 0.025;
CM_0 = 0;
CM_t_0 = 0;

% If a constant name ends with _d ==> it's time derivatived
% Aerodynamic derivatives
CM_u = -0.1;
CM_alpha = -1;
CM_alpha_d = -1.6;
CM_q = -12.5;
CM_t_u = 0;
CM_t_alpha = 0;

CL_u = 0;
CL_alpha = 4;
CL_alpha_d = 0;
CL_q = 0;

CD_u = 0;
CD_alpha = 0.8;
CD_alpha_d = 0;
CD_q = 0;

CT_u = 0;

CM_delta_e = -1.5;
CL_delta_e = 1.1;
CD_delta_e = 0;

CM_delta_T = 0;
CL_delta_T = 0;
CD_delta_T = 0;

% Other
q_bar = 0.5 * air_density * U0^2;

%% Export constants
clear M_verif;
save("constants.mat");