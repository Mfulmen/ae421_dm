clear all; clc; close all;

load('constants.mat')

%% q2
coef1 = q_bar * S / (m * U0);
coef2 = q_bar * S * c / (I_yy * U0);

X_u = -coef1 * (2 * CD0 + C_D_u);
X_w = coef1 * (CL0 - (2 / (pi * epsilon * AR) * CL0 * C_L_alpha));

Z_u = -coef1 * (2 * CL0 + M^2 / (1 - M^2) * CL0);
Z_w = -coef1 * (CD0 + C_L_alpha);
Z_q = coef1 / 2 * C_L_q;

M_u = coef2 * M * Cm_u;
M_w_dot = q_bar * S * c^2 / (2 * I_yy * U0^2) * Cm_alpha_dot;
M_w = coef2 * Cm_alpha;
M_q = q_bar * S * c^2 / (2 * I_yy * U0) * C_m_q;

A = [X_u, X_w, 0, -g*cosd(theta_0); ... Aircraft matrix
     Z_u, Z_w, U0, -g*sind(theta_0); ...
     M_u + M_w_dot * Z_u, M_w + M_w_dot * Z_w, M_q + U0 * M_w_dot, -M_w_dot * g * sind(theta_0);...
     0, 0, 1, 0];
 
disp(A);

X_delta_e = coef1 * C_D_delta_e;
X_delta_T = coef1 * C_D_delta_t;

Z_delta_e = coef1 * C_L_delta_e;
Z_delta_T = coef1 * C_L_delta_t;

M_delta_e = coef2 * C_m_delta_e;
M_delta_T = coef2 * C_m_delta_t;

B = [[X_delta_e, X_delta_T];
    [Z_delta_e, Z_delta_T];
    [M_delta_e + M_w_dot * Z_delta_e, M_delta_T + M_w_dot * Z_delta_T];
    [0, 0]];
disp("Matrix B");
disp(B)

%% Charasteristic equations
[size_A, ~] = size(A); 
coeff_poly = poly(A);
syms x;
polynomial = vpa(det(x * eye(size_A) - A), size_A);

disp("");
disp("Question 3 -------------------");
disp("Characteristic equation:");
disp(polynomial);

%% Eigenvalues
eigenvalues = eig(A);
[eigvec, eigval] = eig(A);

disp("Question 4 -------------------");
disp("Eigenvalues:");
disp(eigenvalues);

ph_id = 3;
sp_id = 1;

ph = eigenvalues(ph_id);
sp = eigenvalues(sp_id);

% Recall: 
% - Eigenvalues of A define stability of x_d = A * x
% - A is 4x4 so A has 4 eigenvalues
% - Stable if eigenvalues all have negative real part
% - The eigenvalues couple that have the smallest abs real part are the phugoid

%% Oscillation modes
% Short period mode
eta_sp = sqrt(1 / (1 + (imag(sp) / real(sp))^2));
omega_sp = -real(sp) / eta_sp;

% Phugoid mode
A_ph = [X_u -g; -Z_u / U0 0]; % Matrix for the phugoid
A_ph_poly = poly(A_ph); % This define the polynome of the phugoid mode

eta_ph = sqrt(1 / (1 + (imag(ph) / real(ph))^2));
omega_ph = -real(ph) / eta_ph;

disp("");
disp("Question 5 -------------------");
disp("Short period mode:");
disp(['  Natural frenquency: ', num2str(omega_sp), ' rad/s; Damping factor: ', num2str(eta_sp)]); 
disp("Phugoid mode:");
disp(['  Natural frenquency: ', num2str(omega_ph), ' rad/s; Damping factor: ', num2str(eta_ph)]);

%% Curves
% Short period mode
t_S = 0:0.01:10;
X_S = eigvec(:, sp_id) * exp(eigval(sp_id, sp_id) * t_S) + ....
      eigvec(:, sp_id + 1) * exp(eigval(sp_id + 1, sp_id + 1) * t_S);

figure;
hold on;
plot(t_S, X_S);
title('Short periode mode')
legend('Axial velocity', 'Angle of attack', 'Pitch angle', 'Pitch rate')
xlabel('Time (sec)')
ylabel('State variable');

t_P = 0:0.1:500;
X_P = eigvec(:, ph_id) * exp(eigval(ph_id, ph_id) * t_P) + ...
      eigvec(:, ph_id + 1) * exp(eigval(ph_id + 1, ph_id + 1) * t_P);

X_P_s = eigvec(:, sp_id) * exp(eigval(sp_id, sp_id) * t_P) + ....
        eigvec(:, sp_id + 1) * exp(eigval(sp_id + 1, sp_id + 1) * t_P) + X_P;
figure()
hold on;
plot(t_P, X_P);
title('Sum of both mode')
legend('Axial velocity', 'Angle of attack', 'Pitch angle', 'Pitch rate')
xlabel('Time (sec)')
ylabel('State variable');

a1 = 12192;
a2_sp = (235.9 + omega_sp * eta_sp * a1) / (omega_sp * sqrt(1 - eta_sp^2));
a2_ph = (235.9 + omega_ph * eta_ph * a1) / (omega_ph * sqrt(1 - eta_ph^2));

X_SP_2 = exp(-omega_sp * eta_sp * t_S) .* (a1 * cos(omega_sp * sqrt(1 - eta_sp^2) * t_S) ...
        + a2_sp * sin(omega_sp * sqrt(1 - eta_sp^2) * t_S));

X_P_2 = exp(-omega_ph * eta_ph * t_P) .* (a1 * cos(omega_ph * sqrt(1 - eta_ph^2) * t_P) ...
        + a2_sp * sin(omega_ph * sqrt(1 - eta_ph^2) * t_P));
%% Transfert functions

s = tf('s');

sI = s * eye(size(A));
sI_A  = sI - A;
inv_si_A = inv(sI - A);

all_tf = minreal(inv_si_A * B)